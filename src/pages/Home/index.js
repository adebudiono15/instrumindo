import React, { useEffect, useState } from 'react'
import { Col, Container, Image, Row } from 'react-bootstrap'
import {IlMaintenence} from '../../assets/ilustrations'
import {ImgLogo} from '../../assets/images'
import styles from './style.module.css'
import PropagateLoader from "react-spinners/PropagateLoader";
import {Helmet} from "react-helmet";

const Index = () => {
const [loading, setLoading] = useState(true)
useEffect(() => {
    setLoading(true);
    setTimeout(() => {
      setLoading(false);
    }, 2000);
  }, []);

  return (
    <div>
        <Helmet>
            <meta charSet="utf-8" />
            <title>Instrumindo</title>
        </Helmet>
        {loading ? (
        <Container style={{ background:'#FFFFFF' }}>
          <Row>
            <Col>
              <Image src={ImgLogo} className={styles.Logo} width={100}/>
              <div className="loadingpage">
              <PropagateLoader size={20} color={"#1F86A9"} loading={loading} />
              </div>
            </Col>
          </Row>
        </Container>
      ) : (
        <div>
        <Container>
            <Row  className={styles.Container}>
                <Col>
                    <h5 className={styles.Text}>Website Sedang Maintenance.</h5>
                    <Image width={300} height={300} src={IlMaintenence}/>
                </Col>
            </Row>
        </Container>
        </div>
      )}
    </div>
  )
}

export default Index